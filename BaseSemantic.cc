//
// Created by alex on 17-8-1.
//

//************************************
// File: BaseSemantic.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 14:39
//************************************

#include "BaseSemantic.h"
#include "logging.h"

namespace dialog {
  BaseSemantic::BaseSemantic() {}

  BaseSemantic::~BaseSemantic() {}
  void BaseSemantic::jieba_segment(string s, vector<string>& words) {
    segment->Cut(s, words, true);
  }

  void BaseSemantic::tag(string s, vector<string> &words, vector<string> &ltp_tag, vector<string> &custom_tag) {
    words.clear();
    ltp_tag.clear();
    custom_tag.clear();

    jieba_segment(s, words);
    postagger_postag(pos_engine, words, ltp_tag);
    for(int i=0; i<words.size(); i++) {
      auto iter=tag_map.find(words[i]);
      if(iter==tag_map.end()) {
        custom_tag.push_back(ltp_tag[i]);
      }else {
        custom_tag.push_back(iter->second);
      }
    }

    INFO(StringFormat(256,"Word Segment result is: %s",VectorToString(words,";").c_str()))
    INFO(StringFormat(256,"LTP Tag result is: %s",VectorToString(ltp_tag,";").c_str()))
    INFO(StringFormat(256,"User Tag result is: %s",VectorToString(custom_tag,";").c_str()))
  }
}