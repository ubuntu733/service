//
// Created by alex on 17-8-1.
//

//************************************
// File: NewsService.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 11:06
//************************************

#include "NewsService.h"

namespace dialog {

  NewsService::NewsService() {}

  NewsService::~NewsService() {}

  json NewsService::getResponse(json content) {
    json wangyi_response;
    //这里注释是默认分类器分类正确,当没有提取到任何新闻关键词时,比如“我想看新闻”这类用户请求时
    //给用户推荐实时热点新闻
/*    if(content.empty()) {
      WARNING(StringFormat("param content is empty"))
      return wangyi_response;
    }*/

    string news_type; //新闻类型
    string news_local;  //新闻城市名称

    auto iter = content.find("TYPE");
    if(iter!=content.end()) {
      news_type = content["TYPE"];
    }else {
      news_type = "";
    }

    iter = content.find("LOCAL");
    if(iter!=content.end()) {
      news_local = content["LOCAL"];
    }else {
      news_local = "";
    }
    INFO(StringFormat(256,"news_type: %s, news_local: %s",news_type.c_str(),news_local.c_str()))
    string url="";
    if(!news_type.empty() && !news_local.empty()) {
      url += "http://c.3g.163.com/hisense/nc/article/local/";
      url +=  news_local;
      url += "/0-20.html";
    }else if(!news_local.empty() && news_type.empty()) {
      url += "http://c.3g.163.com/hisense/nc/article/local/";
      url +=  news_local;
      url += "/0-20.html";
    }else if(news_local.empty() && !news_type.empty()) {
      NewsSemantic* newsSemantic = NewsSemantic::getInstance();
      string tid = newsSemantic->type2tid(news_type);
      if(tid.empty()) {
        url = "http://c.3g.163.com/hisense/nc/article/list/T1348647909107/0-20.html";
      }else {
        url = "http://c.3g.163.com/hisense/nc/article/list/" + tid +"/0-20.html";
      }
    }else {
      url = "http://c.3g.163.com/hisense/nc/article/list/T1348647909107/0-20.html";
    }
    INFO(StringFormat(256,"WangYi News API url: %s",url.c_str()))
    wangyi_response=getResultFromAPI(url);
    return wangyi_response;
  }


  json NewsService::Service(string content) {
    NewsSemantic* newsSemantic = NewsSemantic::getInstance();
    json keywords_json = newsSemantic->analysis(content);
    json wangyiResponse_json = getResponse(keywords_json);
    vector<json> newsAbstract = wangyiResponse_json.front();
    json result_json;
    if(newsAbstract.empty()) {
      return result_json;
    }
    json tmp;
    for(auto iter=newsAbstract.begin(); iter!=newsAbstract.end(); iter++) {
      tmp = *iter;
      result_json.push_back(tmp["title"]);
    }
    return result_json;
  }
}