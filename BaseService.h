//
// Created by alex on 17-7-28.
//

//************************************
// File: BaseService.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-7-28 14:28
//************************************

#ifndef DIALOG_BASESERVICE_H
#define DIALOG_BASESERVICE_H
#include <json.hpp>
#include <string>
using namespace std;
using json = nlohmann::json;
namespace dialog {
  class BaseService {
  public:
    virtual string getResponse(string request) = 0;
  };
}

#endif //DIALOG_BASESERVICE_H
