//
// Created by alex on 17-8-3.
//

//************************************
// File: MusicService.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-3 19:55
//************************************

#ifndef TEST_RESTFUL_MUSICSERVICE_H
#define TEST_RESTFUL_MUSICSERVICE_H

#include "MusicSemantic.h"
#include "WebUtil.h"
namespace dialog {
  class MusicService {
  public:
    MusicService();

    ~MusicService();

    json Service(string content);

  private:
    json getResponse(json topic_words);
  };
}

#endif //TEST_RESTFUL_MUSICSERVICE_H
