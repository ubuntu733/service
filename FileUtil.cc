//
// Created by alex on 17-7-23.
//

//************************************
// File: FileUtil.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-7-23 17:59
//************************************

#include "FileUtil.h"
namespace dialog {
  bool Read(const string& filePath, vector<string>& lineVec) {
    lineVec.clear();
    ifstream ifs(filePath);
    if(!ifs.is_open()) {
      return false;
    }

    string line;
    for(;getline(ifs,line);) {
      lineVec.push_back(line);
    }

    ifs.close();
    return true;
  }

  bool Read(const string& filePath, string& content) {
    content.clear();
    ifstream ifs(filePath);
    if(!ifs.is_open()) {
      return false;
    }
    string line;
    for(;getline(ifs,line);) {
      content.append(line);
    }

    ifs.close();
    return true;
  }

  bool Write(const string& filePath, const string& content, bool overWrite) {
    ofstream ofs;
    if(overWrite) {
      ofs.open(filePath);
    } else {
      ofs.open(filePath,ios::app);
    }

    if(!ofs.is_open()) {
      return false;
    }
    ofs<<content<<"\n";
    ofs.close();
    return true;

  }
}
