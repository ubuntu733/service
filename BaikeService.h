//
// Created by alex on 17-7-28.
//

//************************************
// File: BaikeService.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-7-28 14:43
//************************************

#ifndef DIALOG_BAIKESERVICE_H
#define DIALOG_BAIKESERVICE_H


#include <string>
#include <curl/curl.h>
#include "BaikeSemantic.h"
#include "WebUtil.h"
using namespace std;

namespace dialog {
  class BaikeService  {

  public:
    BaikeService();

    ~BaikeService();

    /**
     * 百科服务主接口
     * @param content 用户请求
     * @return tts
     */
    string service(string content);
  private:
    /**@keyword 百科关键词
     * 请求百度接口,获取结果
     *
     * @return 百度接口返回json
     */
    json getResponse(string keyword);


    /**
     * 分析接口返回数据
     * @param response 接口返回数据
     * @return TTS语句
     */
    string analyseResponse(json response);


  private:
    BaikeSemantic* baikeSemantic;

  };

}
#endif //DIALOG_BAIKESERVICE_H
