//
// Created by alex on 17-8-2.
//

//************************************
// File: MovieService.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-2 20:07
//************************************

#ifndef TEST_RESTFUL_MOVIESERVICE_H
#define TEST_RESTFUL_MOVIESERVICE_H

#include "MovieSemantic.h"
namespace dialog {
  class MovieService {
  public:
    MovieService();

    ~MovieService();

    json Service(string content);

  private:


    /** 请求elasticSearch影视接口,获取结果
    * @content
    * @return
    */
    json getResponse(json post_params);

    /**
     * MovieSemantic 语义解析返回的json对象
     * @param MovieSemanticResult_json
     * @return
     */
    json getPostParams(json MovieSemanticResult_json);
  };

}

#endif //TEST_RESTFUL_MOVIESERVICE_H
