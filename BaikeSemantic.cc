//
// Created by alex on 17-7-28.
//

//************************************
// File: BaikeSemantic.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-7-28 14:53
//************************************

#include "BaikeSemantic.h"

namespace dialog{


  BaikeSemantic *BaikeSemantic::instance = 0;

  BaikeSemantic* BaikeSemantic::getInstance() {
    if(instance == NULL) {
      instance = new BaikeSemantic();
      instance->init(MOVIE_TAG_PATH);
    }
    return instance;
  }
  BaikeSemantic::BaikeSemantic() {

  }

  BaikeSemantic::~BaikeSemantic() {
    postagger_release_postagger(pos_engine);
  }



  string BaikeSemantic::analysis(string content) {

    INFO("Begin to analysis Baike Service");
    string result;
    if (content.empty()) {
      WARNING(StringFormat(256,"param %s is empty","content"));
      result = "";
      return result;
    }

    result = getTopicWord(content);
    if(result.empty()) {
      WARNING("getTopicWord error");
      result = "";
      return result;
    }
    return result;

  }

  string BaikeSemantic::getTopicWord(string content) {

    INFO("Begin to get topic words");
    if(content.empty()) {
      WARNING(StringFormat(256,"param %s is empty","content"));
      return "";
    }

    //目前只支持电影名,人名百科,默认只有只查找词性中的第一个词性
    vector<string> words;
    vector<string> ltp_tags;
    vector<string> tags;
    tag(content,words,ltp_tags,tags);

    for(int i=0; i<words.size(); i++) {
      if(tags[i]=="nh" || tags[i]=="movie") {
        INFO(StringFormat(256,"find Baike topic word %s,tag is %s",words[i].c_str(),tags[i].c_str()))
        return words[i];
      }
    }
    WARNING("can not find Baike topic word")
    return "";
    
  }

  void BaikeSemantic::init(string filePath) {
    INFO("Begin to init BaikeSemantic instance")
    segment = new cppjieba::MixSegment(DICT_PATH, HMM_PATH, USER_DICT_PATH);
    pos_engine = postagger_create_postagger(pos_model.c_str());
    ifstream ifs(filePath);
    if (!ifs.is_open()) {
      WARNING(StringFormat(256,"file %s open failed",filePath.c_str()))
      return;
    }
    size_t linenum = 0;
    string line;
    for (; getline(ifs, line); linenum++) {
      string s1, s2;
      Split(line, "\t", s1, s2);
      tag_map.insert(pair<string, string>(s1, s2));
    }
  }


}