//
// Created by alex on 17-8-3.
//

//************************************
// File: MusicSemantic.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-3 14:29
//************************************

#include "MusicSemantic.h"

namespace dialog {
  MusicSemantic* MusicSemantic::instance = 0;

  MusicSemantic::MusicSemantic() {}

  MusicSemantic::~MusicSemantic() {}

  MusicSemantic* MusicSemantic::getInstance() {
    if(instance==NULL) {
      instance = new MusicSemantic();
      instance->init(MUSIC_TAG_PATH);
    }
    return instance;
  }

  void MusicSemantic::init(string filePath) {
    INFO("Begin to init MusicSemantic instance")
    segment = new cppjieba::MixSegment(DICT_PATH.c_str(), HMM_PATH.c_str(), USER_DICT_PATH.c_str());
    pos_engine = postagger_create_postagger(pos_model.c_str());
    ifstream ifs(filePath);
    if (!ifs.is_open()) {
      WARNING(StringFormat(256,"file %s open failed",filePath.c_str()))
      return;
    }
    size_t linenum = 0;
    string line;
    for (; getline(ifs, line); linenum++) {
      string s1, s2;
      Split(line, " ", s1, s2);
      tag_map.insert(pair<string, string>(s1, s2));
    }
  }

  json MusicSemantic::analysis(string content) {
    json topicWords_json;
    INFO("Begin to get Music topic words");
    if(content.empty()) {
      WARNING(StringFormat(256,"param %s is empty","content"));
      return topicWords_json;
    }

    vector<string> words;
    vector<string> ltp_tags;
    vector<string> tags;
    tag(content,words,ltp_tags,tags);

    for(int i=0; i<words.size(); i++) {
      if(tags[i]=="music") {
        INFO(StringFormat(128,"Find Music Name topic word:%s",words[i].c_str()).c_str())
        topicWords_json["MUSIC"] = words[i];
      }else if(tags[i]=="nh") {
        INFO(StringFormat(128,"Find Singer Name topic word:%s",words[i].c_str()).c_str())
        topicWords_json["SINGER"] = words[i];
      }
    }
    if(topicWords_json.empty()) {
      WARNING("Can not find any music topic word");
    }
      return topicWords_json;

  }
}