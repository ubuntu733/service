//
// Created by alex on 17-8-3.
//

//************************************
// File: MusicSemantic.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-3 14:29
//************************************

#ifndef TEST_RESTFUL_MUSICSEMANTIC_H
#define TEST_RESTFUL_MUSICSEMANTIC_H

#include "BaseSemantic.h"
#include <json.hpp>
#include "logging.h"
#include "StringUtil.h"
using json = nlohmann::json;
namespace  dialog {
  class MusicSemantic : public BaseSemantic {
  public:

    static MusicSemantic* getInstance();

    json analysis(string content);

  private:
    MusicSemantic();
    ~MusicSemantic();

    void init(string filePath);

  private:
    static MusicSemantic* instance;
  };

}


#endif //TEST_RESTFUL_MUSICSEMANTIC_H
