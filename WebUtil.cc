//
// Created by alex on 17-8-1.
//

//************************************
// File: WebUtil.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 11:19
//************************************

#include "WebUtil.h"

namespace dialog {
  json getResultFromAPI(string url, bool post, string post_params) {
    CURL *curl;
    CURLcode res;
    string result;
    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();
    if(curl) {
      curl_easy_setopt(curl, CURLOPT_URL,url.c_str());
      if(post) {
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_params.c_str());
      }
      curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,CurlWrite_CallbackFunc_StdString);
      curl_easy_setopt(curl,CURLOPT_WRITEDATA,&result);

      /* Perform the request, res will get the return code */
      res = curl_easy_perform(curl);

      /* Check for errors */
      if(res != CURLE_OK)
        WARNING(StringFormat("curl_easy_perform() failed:%s",curl_easy_strerror(res)));

      /* always cleanup */
      curl_easy_cleanup(curl);
    }

    curl_global_cleanup();
    json j;
    if(!result.empty())
      j = json::parse(result);
    return j;

  }


  size_t CurlWrite_CallbackFunc_StdString(void *contents, size_t size, size_t nmemb, std::string *s)
  {
    size_t newLength = size*nmemb;
    size_t oldLength = s->size();
    try
    {
      s->resize(oldLength + newLength);
    }
    catch(std::bad_alloc &e)
    {
      //handle memory problem
      return 0;
    }

    std::copy((char*)contents,(char*)contents+newLength,s->begin()+oldLength);
    return size*nmemb;
  }


  json chatting(string content) {
    string url = "http://www.tuling123.com/openapi/api";
    string post_params = "key=ad0dc926c78e49079b23b3450dd25448&info=";
    post_params += content;
    return getResultFromAPI(url, true, post_params);
  }
}