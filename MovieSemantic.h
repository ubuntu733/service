//
// Created by alex on 17-8-2.
//

//************************************
// File: MovieSemantic.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-2 19:29
//************************************

#ifndef TEST_RESTFUL_MOVIESEMANTIC_H
#define TEST_RESTFUL_MOVIESEMANTIC_H

#include <json.hpp>
#include <string>
#include "cppjieba/Jieba.hpp"
#include <unordered_map>
#include "ltp/postag_dll.h"
#include "Common.h"
#include "logging.h"
#include "StringUtil.h"
#include "BaseSemantic.h"

using json = nlohmann::json;
using namespace std;

namespace dialog {
  class MovieSemantic : public BaseSemantic{
  public:
    static MovieSemantic* getInstance();

    json analysis(string content);

  private:
    void init(string filePath);

    MovieSemantic();

    ~MovieSemantic();

  private:
    static MovieSemantic* instance;
  };
}


#endif //TEST_RESTFUL_MOVIESEMANTIC_H
