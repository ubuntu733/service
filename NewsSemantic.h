//
// Created by alex on 17-8-1.
//

//************************************
// File: NewsSemantic.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 11:31
//************************************

#ifndef DIALOG_NEWSSEMANTIC_H
#define DIALOG_NEWSSEMANTIC_H

#include <json.hpp>
#include <string>
#include "cppjieba/Jieba.hpp"
#include <unordered_map>
#include "ltp/postag_dll.h"
#include "Common.h"
#include "logging.h"
#include "StringUtil.h"
#include "BaseSemantic.h"

using json = nlohmann::json;
using namespace std;
namespace dialog {
  class NewsSemantic:public BaseSemantic{
  public:
    static NewsSemantic *getInstance();

    json analysis(string content);

    string type2tid(string type);
  private:
    /**
     * 用户请求预处理
     * @param content
     * @return
     */
    string preProcess(string content);


    void init(string filePath);

    NewsSemantic();

    ~NewsSemantic();

  private:

    static NewsSemantic *instance;
    map<string, string> synonyms_map; //同义词
    map<string, string> newsType_map; //新闻类型对应Tid

  };
}

#endif //DIALOG_NEWSSEMANTIC_H
