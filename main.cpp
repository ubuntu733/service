/**************************************************************************
 *                                  _   _ ____  _
 *  Project                     ___| | | |  _ \| |
 *                             / __| | | | |_) | |
 *                            | (__| |_| |  _ <| |___
 *                             \___|\___/|_| \_\_____|
 *
 * Copyright (C) 1998 - 2015, Daniel Stenberg, <daniel@haxx.se>, et al.
 *
 * This software is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at https://curl.haxx.se/docs/copyright.html.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the COPYING file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ***************************************************************************/
/* <DESC>
 * Simple HTTPS GET
 * </DESC>
 */
#include "crow.h"
#include <stdio.h>
#include <curl/curl.h>
#include <string>
#include <iostream>
#include <cctype>
#include <string.h>
#include <iomanip>
#include <sstream>
#include <json.hpp>
#include "BaikeService.h"
#include "StringUtil.h"
#include "NewsService.h"
#include "Common.h"
#include "WebUtil.h"
#include "MovieSemantic.h"
#include "MovieService.h"
#include "MusicSemantic.h"
#include "MusicService.h"
#include "logging.h"

using json = nlohmann::json;


using namespace dialog;



int main()
{
//  std::mutex mtx;
  crow::SimpleApp app;

  CROW_ROUTE(app, "/params")
          ([](const crow::request& req){

            std::ostringstream os;

            std::string typeId = req.url_params.get("typeId");
            string content = req.url_params.get("content");
            INFO(StringFormat(256,"typeId: %s, content: %s",typeId.c_str(),content.c_str()));
            //1:影视,2:音乐,3:新闻,4:天气,5:聊天
            if(typeId=="1") {
              MovieService movieService;
              os << movieService.Service(content);
            }else if(typeId=="2") {
              MusicService musicService;
              os << musicService.Service(content);
            }else if(typeId=="3") {
              NewsService newsService;
              os << newsService.Service(content);
            }else if(typeId=="4") {
              os << chatting(content);
            }else if(typeId=="5") {
              os << chatting(content);
            }else {
              os << "找不到对应的服务";
            }

       //     s = s.substr(1,s.size()-2);
            INFO(os.str());
            return crow::response{os.str()};
          });

  app.port(18081).run();
  google::ShutdownGoogleLogging();
}
