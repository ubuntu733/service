//
// Created by alex on 17-8-2.
//

//************************************
// File: MovieService.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-2 20:07
//************************************

#include "MovieService.h"
#include "WebUtil.h"

namespace dialog {

  MovieService::MovieService() {}

  MovieService::~MovieService() {}

  json MovieService::getPostParams(json MovieSemanticResult_json) {
    json postParams_json;
    if(MovieSemanticResult_json.empty()) {
      WARNING("param MovieSemanticResult_json is empty")
      return postParams_json;
    }
    string movie_name,movie_type,person_name;
    auto iter=MovieSemanticResult_json.find("MOVIE_NAME");
    if(iter!=MovieSemanticResult_json.end()) {
      movie_name = MovieSemanticResult_json["MOVIE_NAME"];
    }else {
      movie_name = "";
    }

    iter=MovieSemanticResult_json.find("MOVIE_TYPE");
    if(iter!=MovieSemanticResult_json.end()) {
      movie_type = MovieSemanticResult_json["MOVIE_TYPE"];
    }else {
      movie_type = "";
    }

    iter=MovieSemanticResult_json.find("PERSON_NAME");
    if(iter!=MovieSemanticResult_json.end()) {
      person_name = MovieSemanticResult_json["PERSON_NAME"];
    }else {
      person_name = "";
    }

    postParams_json["stored_fields"] = "NAME";
    string params_string;
    if(!person_name.empty() && !movie_name.empty() && movie_type.empty()) {
      //包含人名和影视名
      postParams_json["query"]["query_string"]["query"] = StringFormat(128,"NAME:%s +YANYUAN:%s",movie_name.c_str(),person_name.c_str());
      return postParams_json;
    }

    if(!person_name.empty() && movie_name.empty() && !movie_type.empty()) {
      //包含人名和类型名
      postParams_json["query"]["query_string"]["query"] = StringFormat(128,"LEIXING:%s +YANYUAN:%s",movie_type.c_str(),person_name.c_str());
      return postParams_json;
    }
    if(!person_name.empty() && movie_name.empty() && movie_type.empty()) {
      //只包含人名
      params_string = "YANYUAN:"+person_name;
      cout <<StringFormat(128,"YANYUAN: %s",person_name.c_str())<<endl;
      postParams_json["query"]["query_string"]["query"] = StringFormat(128,"YANYUAN: %s",person_name.c_str());
      return postParams_json;
    }

    if(person_name.empty() && !movie_name.empty() && movie_type.empty()) {
      //只包含影视名
      postParams_json["query"]["query_string"]["query"] = StringFormat(128,"NAME:%s",movie_name.c_str());
      return postParams_json;
    }

    if(person_name.empty() && movie_name.empty() && !movie_type.empty()) {
      //只包含类型名
      postParams_json["query"]["query_string"]["query"] = StringFormat(128,"FENLEI:%s",movie_type.c_str());
      return postParams_json;
    }

    return postParams_json;
  }

  json MovieService::getResponse(json post_params) {
    json es_json;
    if(post_params.empty()) {
      WARNING("param post_params is empty")
      return es_json;
    }

    string url = "10.18.203.112:9200/index/test3/_search?pretty=true";
    es_json = getResultFromAPI(url,true,post_params.dump());
    return es_json;
  }

  json MovieService::Service(string content) {
    json result_json;
    if(content.empty()) {
      WARNING("param content is empty")
      return result_json;
    }

    MovieSemantic* movieSemantic = MovieSemantic::getInstance();
    json MovieSemanticResult_json = movieSemantic->analysis(content);
    if(MovieSemanticResult_json.empty()) {
      WARNING("MovieSemanticResult_json is empty")
      return result_json;
    }

    json postParams_json = getPostParams(MovieSemanticResult_json);
    if(postParams_json.empty()) {
      WARNING("postParams is empty")
      return result_json;
    }
    cout << postParams_json.dump()<<endl;
    json es_json = getResponse(postParams_json);

    if(es_json["hits"]["total"]>0) {
      vector<json> hits = es_json["hits"]["hits"];
      for(auto iter=hits.begin(); iter!=hits.end(); iter++) {
        result_json.push_back((*iter)["fields"]["NAME"]);
      }
      return result_json;
    } else {
      WARNING("Can not find any movie")
      return es_json;
    }

  }
}