//
// Created by alex on 17-8-1.
//

//************************************
// File: WenUtil.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 11:16
//************************************

#ifndef DIALOG_WENUTIL_H
#define DIALOG_WENUTIL_H

#include <json.hpp>
#include <string>
using namespace std;
using json = nlohmann::json;
#include <curl/curl.h>
#include "logging.h"

namespace dialog {

  json getResultFromAPI(string url, bool post=false, string post_params="");

  static size_t CurlWrite_CallbackFunc_StdString(void *contents, size_t size, size_t nmemb, std::string *s);

  json chatting(string content);
}
#endif //DIALOG_WENUTIL_H
