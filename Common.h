//
// Created by alex on 17-8-1.
//

//************************************
// File: Common.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 13:50
//************************************

#ifndef DIALOG_COMMON_H
#define DIALOG_COMMON_H

#include <string>
using namespace std;
namespace dialog {
  extern string ROOT_PATH;
  extern string DICT_PATH;
  extern string HMM_PATH;
  extern string USER_DICT_PATH;
  extern string pos_model;
  extern string user_dict;
  extern string MOVIE_TAG_PATH;
  extern string NEWS_TAG_PATH;
  extern string NEWS_TYPE_PATH;
  extern string MUSIC_DICT_PATH;
  extern string MUSIC_TAG_PATH;
}
#endif //DIALOG_COMMON_H
