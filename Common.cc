//
// Created by alex on 17-8-1.
//

//************************************
// File: Common.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 13:50
//************************************

#include "Common.h"
#include "string"
using namespace std;
namespace dialog {
  string ROOT_PATH = "/Users/chenchengen/Source/ClionProjects/service/";
  string DICT_PATH = ROOT_PATH+"data/dict/jieba.dict.utf8";
  string HMM_PATH = ROOT_PATH+"data/dict/hmm_model.utf8";
  string USER_DICT_PATH = "/Users/chenchengen/Source/ClionProjects/service/data/dict/user.dict.utf8|/Users/chenchengen/Source/ClionProjects/service/data/dict/movie_dict.txt|/Users/chenchengen/Source/ClionProjects/service/data/dict/music_dict.utf8";
  string pos_model =ROOT_PATH+ "data/dict/pos.model";
// const char *const user_dict = "/home/alex/ClionProjects/context/ltp_dict.txt";
  string MOVIE_TAG_PATH = ROOT_PATH+"data/dict/movie_tag.txt";
  string NEWS_TAG_PATH = ROOT_PATH+"data/dict/News_dict.utf8";
  string NEWS_TYPE_PATH = ROOT_PATH+"data/dict/News_type.utf8";
  string MUSIC_DICT_PATH = ROOT_PATH+"data/dict/music_dict.utf8";
  string MUSIC_TAG_PATH = ROOT_PATH+"data/dict/music_type.utf8";
}