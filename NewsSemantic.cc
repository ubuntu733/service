//
// Created by alex on 17-8-1.
//

//************************************
// File: NewsSemantic.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 11:31
//************************************

#include "NewsSemantic.h"

namespace dialog {
  NewsSemantic *NewsSemantic::instance = 0;

  NewsSemantic* NewsSemantic::getInstance() {
    if(instance==NULL) {
      instance = new NewsSemantic();
      instance->init(NEWS_TAG_PATH);
    }
    return instance;
  }

  NewsSemantic::NewsSemantic() {

  }

  NewsSemantic::~NewsSemantic() {}

  void NewsSemantic::init(string filePath) {
    INFO("Begin to init BaikeSemantic instance")
    segment = new cppjieba::MixSegment(DICT_PATH, HMM_PATH, USER_DICT_PATH);
    pos_engine = postagger_create_postagger(pos_model.c_str());
    ifstream ifs(filePath);
    if (!ifs.is_open()) {
      WARNING(StringFormat(256,"file %s open failed",filePath.c_str()))
      return;
    }
    size_t linenum = 0;
    string line;
    for (; getline(ifs, line); linenum++) {
      string s1, s2;
      Split(line, " ", s1, s2);
      tag_map.insert(pair<string, string>(s1, s2));
    }
    ifs.close();

    ifs.open(NEWS_TYPE_PATH);
    if (!ifs.is_open()) {
      WARNING(StringFormat(256,"file %s open failed",filePath.c_str()))
      return;
    }
    linenum = 0;
    line.clear();
    for (; getline(ifs, line); linenum++) {
      string s1, s2;
      Split(line, " ", s1, s2);
      newsType_map.insert(pair<string, string>(s1, s2));
    }
    ifs.close();
  }


  string NewsSemantic::type2tid(string type) {
    if(type.empty()) {
      WARNING("param type is empty")
      return "";
    }
    auto iter = newsType_map.find(type);
    if(iter==newsType_map.end()) {
      WARNING(StringFormat(256,"Cannot find appropriate tid with newsType:%s",type.c_str()));
      return "";
    }
    INFO(StringFormat(256,"find appropriate tid with newsType:%s",type.c_str()))
    return iter->second;
  }

  json NewsSemantic::analysis(string content) {
    json result_json;
    INFO("Begin to analysis News Semantic")
    if(content.empty()) {
      WARNING("param content is NULL")
      return result_json;
    }

    vector<string> words;
    vector<string> ltp_tags;
    vector<string> tags;
    tag(content,words,ltp_tags,tags);

    for(int i=0; i<words.size(); i++) {
      if(tags[i]=="ns") {
        INFO(StringFormat(128,"Find local:%s",words[i].c_str()))
        result_json["LOCAL"]=words[i];
        return result_json;
      }

      if(tags[i]=="newsType") {
        INFO(StringFormat(128,"Find newsType: %s",words[i].c_str()))
        result_json["TYPE"]=words[i];
        return result_json;
      }
    }

    WARNING("Can not find local or newsType")
    return result_json;

  }
}