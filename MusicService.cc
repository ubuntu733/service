//
// Created by alex on 17-8-3.
//

//************************************
// File: MusicService.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-3 19:55
//************************************

#include "MusicService.h"

namespace dialog {

  MusicService::MusicService() {}

  MusicService::~MusicService() {}

  json MusicService::getResponse(json topic_words) {
    json result_json;
    if(topic_words.empty()) {
      WARNING("param topic_words is empty")
      return result_json;
    }
    string url = "http://tingapi.ting.baidu.com/v1/restserver/ting?format=json&calback=&from=webapp_music&"
            "method=baidu.ting.search.catalogSug&query=";
    string music,singer;
    //查找是否有歌手名,或者音乐名
    auto iter = topic_words.find("MUSIC");
    if(iter!=topic_words.end()) {
      music = *(iter);
    }else {
      music = "";
    }

    iter = topic_words.find("SINGER");
    if(iter!=topic_words.end()) {
      singer = *(iter);
    }else {
      singer = "";
    }

    //优先查找音乐名
    if((!music.empty() && !singer.empty()) || (!music.empty() && singer.empty())) {
      url += music;
    }else if(music.empty() && !singer.empty()) {
      url += singer;
    }

    result_json = getResultFromAPI(url);
    return result_json;
  }

  json MusicService::Service(string content) {
    json result_json;
    if(content.empty()) {
      WARNING("param content is empty")
      return result_json;
    }

    MusicSemantic* musicSemantic = MusicSemantic::getInstance();
    //获取音乐主题词,歌手或者音乐名
    json topicWords_json = musicSemantic->analysis(content);

    //从百度接口获取返回json对象
    json BaiduResponse_json = getResponse(topicWords_json);

    //从返回的信息中抽取需要的信息,包括歌名和歌手
    vector<json> tmp = BaiduResponse_json["song"];
    for(auto iter=tmp.begin(); iter!=tmp.end(); iter++) {
      json response;
      response["songname"]=(*iter)["songname"];
      response["artistname"]=(*iter)["artistname"];
      result_json.push_back(response);
    }
    return result_json;
  }
}