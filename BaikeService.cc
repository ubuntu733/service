//
// Created by alex on 17-7-28.
//

//************************************
// File: BaikeService.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-7-28 14:43
//************************************

#include "BaikeService.h"

namespace dialog{
  BaikeService::BaikeService() {
    baikeSemantic = BaikeSemantic::getInstance();
  }

  BaikeService::~BaikeService() {};

  string BaikeService::service(string content) {
    string keyword = baikeSemantic->analysis(content);
    json j = getResponse(keyword);
    return j["abstract"];
  }
  json BaikeService::getResponse(string keyword) {
    json token_json = getResultFromAPI("https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=60IlNKsRO3YWIVhvxTMofGSQ&client_secret=IOY5yVUHGWEE8keay7RpWrwxvznTeGAk&");
    string token = token_json["access_token"];
    string Baike_url ="https://openapi.baidu.com/rest/2.0/baike/openapi/BaikeLemmaCardApi?access_token="+token+"&format=json&appid=869366&bk_key=";

    Baike_url += keyword+"&bk_length=400";
    json baikeResult_json = getResultFromAPI(Baike_url);
    return baikeResult_json;
  }



}