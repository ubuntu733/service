//
// Created by alex on 17-8-1.
//

//************************************
// File: BaseSemantic.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 14:39
//************************************

#ifndef DIALOG_BASESEMANTIC_H
#define DIALOG_BASESEMANTIC_H

#include <string>
#include <vector>
#include "cppjieba/Jieba.hpp"
#include "ltp/postag_dll.h"
#include "Common.h"
#include <unordered_map>
using namespace std;
namespace dialog {
  class BaseSemantic {
  public:
    BaseSemantic();

    virtual ~BaseSemantic();

    void jieba_segment(string s, vector<string>& words);

    void tag(string s, vector<string> &words, vector<string> &ltp_tag, vector<string> &custom_tag);

    virtual void init(string filePath) =0;

  protected:
    cppjieba::MixSegment *segment;    //结巴分词
    void *pos_engine;   //ltp词性标注
    unordered_map<string, string> tag_map;
  };
}


#endif //DIALOG_BASESEMANTIC_H
