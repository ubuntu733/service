//
// Created by alex on 17-7-28.
//

//************************************
// File: BaikeSemantic.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-7-28 14:53
//************************************

#ifndef DIALOG_BAIKESEMANTIC_H
#define DIALOG_BAIKESEMANTIC_H

#include <map>
#include <json.hpp>
#include "logging.h"
#include "StringUtil.h"
#include "cppjieba/Jieba.hpp"
#include "ltp/postag_dll.h"
#include "BaseSemantic.h"

using json = nlohmann::json;
using namespace std;
namespace dialog {
  class BaikeSemantic: public BaseSemantic {
  public:


    /**
     * 百科接口服务主入口
     * @param content 用户请求
     * @return 百科关键词
     */
    string analysis(string content);

    static BaikeSemantic* getInstance();

  private:
    /**
    * 抽取用户请求中的关键词
    * @param content 用户请求
    */

    string getTopicWord(string content);



    BaikeSemantic();

    ~BaikeSemantic();

    void init(string filePath);





  private:
    static BaikeSemantic* instance;
    map<string,string> dictpath_map; //词典路径
  };
}


#endif //DIALOG_BAIKESEMANTIC_H
