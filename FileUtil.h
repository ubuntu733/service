//
// Created by alex on 17-7-23.
//

//************************************
// File: FileUtil.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-7-23 17:55
//************************************

#ifndef HUOGUONLP_FILEUTIL_H
#define HUOGUONLP_FILEUTIL_H

#include <string>
#include <vector>
#include <fstream>
using namespace std;
namespace dialog {
  bool Read(const string& filePath, vector<string>& lineVec);

  bool Read(const string& filePath, string& content);

  bool Write(const string& filePath, const string& content, bool overWrite=true);
}
#endif //HUOGUONLP_FILEUTIL_H
