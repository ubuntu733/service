//
// Created by alex on 17-8-2.
//

//************************************
// File: MovieSemantic.cc
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-2 19:29
//************************************

#include "MovieSemantic.h"

namespace dialog {
  MovieSemantic *MovieSemantic::instance = 0;

  MovieSemantic::MovieSemantic() {}

  MovieSemantic::~MovieSemantic() {}

  MovieSemantic* MovieSemantic::getInstance() {
    if(instance==NULL) {
      instance = new MovieSemantic();
      instance->init(MOVIE_TAG_PATH);
    }
    return instance;
  }

  void MovieSemantic::init(string filePath) {
    INFO("Begin to init MovieSemantic instance")
    segment = new cppjieba::MixSegment(DICT_PATH, HMM_PATH, USER_DICT_PATH);
    pos_engine = postagger_create_postagger(pos_model.c_str());
    ifstream ifs(filePath);
    if (!ifs.is_open()) {
      WARNING(StringFormat(256,"file %s open failed", filePath.c_str()))
      return;
    }
    size_t linenum = 0;
    string line;
    for (; getline(ifs, line); linenum++) {
      string s1, s2;
      Split(line, "\t", s1, s2);
      tag_map.insert(pair<string, string>(s1, s2));
    }
    ifs.close();



  }

  json MovieSemantic::analysis(string content) {
    json result_json;
    INFO("Begin to analysis MovieSemantic")

    if(content.empty()) {
      WARNING("param content is NULL")
      return result_json;
    }

    vector<string> words;
    vector<string> ltp_tags;
    vector<string> tags;
    tag(content,words,ltp_tags,tags);

    for(int i=0; i<words.size(); i++) {
      if(tags[i]=="movie") {
        INFO(StringFormat(256,"Find MOVIE_NAME: %s",words[i].c_str()).c_str())
        result_json["MOVIE_NAME"]=words[i];
      }

      if(tags[i]=="nh") {
        INFO(StringFormat(256,"Find PERSON_NAME: %s",words[i].c_str()).c_str())
        result_json["PERSON_NAME"]=words[i];
      }

      if(tags[i]=="movie_type") {
        INFO(StringFormat(256,"Find MOVIE_TYPE: %s",words[i].c_str()))
        result_json["MOVIE_TYPE"]=words[i];
      }
    }
    if(result_json.empty()) {
      WARNING("Can not find MOVIE_NAME ,MOVIE_TYPE or PERSION_NAME ")
    }
    return result_json;
  }
}
