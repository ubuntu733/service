//
// Created by alex on 17-8-1.
//

//************************************
// File: NewsService.h
// Autor: ShenChengEn - ubuntu733@gmail.com
// Description: ---
// create: 17-8-1 11:06
//************************************

#ifndef DIALOG_NEWSSERVICE_H
#define DIALOG_NEWSSERVICE_H

#include "NewsSemantic.h"
#include "WebUtil.h"
#include <map>

using namespace std;
namespace dialog {
  class NewsService  {
  public:
    NewsService();
    ~NewsService();

    json Service(string content);

  private:


    /** 请求网易接口,获取结果
     * @content 新闻语义解析返回的json
     * @return 网易接口返回json
     */
    json getResponse(json content);

    /**
     * 同义词词典初始化
     * @param path 同义词词典路径
     */
    void init(string path);

  private:
    map<string, string> synonyms_map;
  };
}


#endif //DIALOG_NEWSSERVICE_H
